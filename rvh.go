package rvh

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/robfig/cron"
)

var rvh string
var wg sync.WaitGroup
var client *http.Client

// Get : get
func Get() string {
	wg.Wait()
	return rvh
}

func get() {
	value, err := getFromDrecom()
	if err == nil {
		set(value)
		return
	}
	log.Println(err)
	value, err = getFromGitLab()
	if err == nil {
		set(value)
		return
	}
	log.Println(err)
	log.Printf("Resource Version Hash remains as %v", rvh)
}

func set(value string) {
	rvh = value
	log.Printf("Resource Version Hash is set as %v", rvh)
}

func getFromGitLab() (string, error) {
	request, err := http.NewRequest("GET", "https://rvh.kirafan.cn/", nil)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from GitLab: %v", err)
	}
	response, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from GitLab: %v", err)
	}
	if response.StatusCode != 200 {
		return "", fmt.Errorf("Resource Version Hash from GitLab: Status code %v", response.StatusCode)
	}
	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from GitLab: %v", err)
	}
	return string(data), nil
}

func getFromDrecom() (string, error) {
	request, err := http.NewRequest("GET", "https://krr-prd.star-api.com/api/app/version/get?platform=1&version=1.19.0", nil)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from Drecom: %v", err)
	}
	response, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from Drecom: %v", err)
	}
	if response.StatusCode != 200 {
		return "", fmt.Errorf("Resource Version Hash from Drecom: Status code %v", response.StatusCode)
	}
	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from Drecom: %v", err)
	}
	var dict map[string]interface{}
	err = json.Unmarshal(data, &dict)
	if err != nil {
		return "", fmt.Errorf("Resource Version Hash from Drecom: %v", err)
	}
	rvh, ok := dict["resourceVersionHash"]
	if !ok {
		return "", fmt.Errorf("Resource Version Hash from Drecom: Key error: resourceVersionHash")
	}
	return rvh.(string), nil
}

func init() {
	client = &http.Client{
		Timeout: 10 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	wg.Add(1)
	go func() {
		defer wg.Done()
		get()
		job := cron.New()
		job.AddFunc("*/15 * * * *", get)
		job.Start()
	}()
}
