package main

import (
	"fmt"

	rvh "gitlab.com/kirafan/resource-version-hash"
)

func main() {
	fmt.Println(rvh.Get())
}
